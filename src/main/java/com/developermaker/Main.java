package com.developermaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final String NAME_JOHN = "John";
    private static Logger logger = LoggerFactory.getLogger(Main.class);
    
    public static void main(String[] args) {
        String greeting = "Hello World";
        logger.debug(greeting + 1);
        String myName = NAME_JOHN;
        System.out.println(namedJohn(myName));
        while(true){
            if(namedJohn(myName)){
                logger.info("I am John");
                break;
            }
        }
    }

    private static boolean namedJohn(String name){
        return NAME_JOHN.equals(name);
    }

}  
